package com.oramind.loggio.model

import com.beust.klaxon.Klaxon

data class AnalyzedLine(
    val lineIndex: Int,
    val content : String,
    val properties: Map<String, String>
) {}

class TreeElement (
    val children : MutableList<TreeElement> = mutableListOf(),
    val value : Any? = null
) {
    fun addChild(treeElement: TreeElement) {
        this.children.add(treeElement)
    }
}

class ScalarValue(value: Any? = null) {}

