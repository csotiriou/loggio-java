package com.oramind.loggio.util

import com.oramind.loggio.model.AnalyzedLine
import org.apache.logging.log4j.LogManager
import org.apache.lucene.document.*
import org.apache.lucene.index.IndexWriter
import org.apache.lucene.index.IndexWriterConfig
import org.apache.lucene.store.MMapDirectory
import java.io.BufferedReader
import java.io.File
import java.io.FileInputStream
import java.io.FileReader
import java.nio.file.Paths

class FileParser {

    private lateinit var stream : FileInputStream

    fun parse(filePath : String, regex: Regex, groupNames : List<String>, contentGroupName : String): MutableList<AnalyzedLine> {
        val fileLines = mutableListOf<AnalyzedLine>()
        this.stream = FileInputStream(filePath)

        val indexWriterConfig = IndexWriterConfig()

        val indexFilePath = Paths.get("$filePath.idx")

        val directory = MMapDirectory(indexFilePath)
        val indexWriter = IndexWriter(directory, indexWriterConfig)

        val fileReader = FileReader(filePath)
        val br = BufferedReader(fileReader)
        var line : String? = null
        var iteration = 0

        do {
            line = br.readLine()
            line?.let { lineNonOptional ->
                val map = mutableMapOf<String, String>()

                val regexResults = regex.findAll(lineNonOptional)
                regexResults.forEach { matchResult ->

                    groupNames.forEach {groupName ->
                        matchResult.groups[groupName]
                            ?.let { matchGroup ->
                                Pair(groupName, matchGroup.value)
                            }?.let { pair ->
                                map.put(pair.first, pair.second)
                        }
                    }
                }

                val document = Document()
                document.add(StoredField("line_number", iteration))
                map.forEach() {
                    val field : Field
                    if (it.key == contentGroupName) {
                        field = TextField(it.key, it.value, Field.Store.NO)
                    } else {
                        field = StringField(it.key, it.value, Field.Store.YES)
                    }
                    document.add(field)
                }
                indexWriter.addDocument(document)

                fileLines.add(AnalyzedLine(iteration, lineNonOptional, map))
                iteration++
            }
        } while (line != null)

        log.info("commited " + iteration + "documents to the index")
        indexWriter.commit()
        indexWriter.close()

        log.info("index finalized commit")

        return fileLines
    }

    companion object {
        private val log = LogManager.getLogger()
    }
}
