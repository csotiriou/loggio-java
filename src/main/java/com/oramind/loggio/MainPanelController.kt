package com.oramind.loggio

import com.oramind.loggio.model.AnalyzedLine
import com.oramind.loggio.util.FileParser
import javafx.collections.FXCollections
import org.apache.logging.log4j.LogManager
import tornadofx.Controller

class MainPanelController : Controller() {
    val mainPanelView : MainPanelView by inject()
    val regexConstructor: RegexConstructorView by inject()

    var fileLines = FXCollections.observableArrayList<AnalyzedLine>()
    var columnNames = FXCollections.observableArrayList<String>()

    fun init() {
        with(config) {
            set(FILE_PATH to "")
        }

        mainPanelView.replaceWith(this.regexConstructor)
    }

    fun reset() {
        this.fileLines.clear()
        this.columnNames.clear()
    }

    fun parse(filePath : String, regex: Regex, contentGroupName : String) {

        val regexGroupNameFinder = """\?\<(.+?)\>""".toRegex()

        val groupNames = regexGroupNameFinder
            .findAll(regex.pattern)
            .map { matchResult ->
                matchResult.groupValues.last()
            }
            .toList()


        val parser = FileParser()
        val parsedResults = parser.parse(filePath, regex, groupNames, contentGroupName)


        this.fileLines.addAll(parsedResults)
        this.columnNames.addAll(parsedResults[0].properties.keys)
        mainPanelView.refreshTable()

    }

    companion object {
        val FILE_PATH = "FILE_PATH"
        private val log = LogManager.getLogger()
    }
}
