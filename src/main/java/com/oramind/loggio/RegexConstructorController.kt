package com.oramind.loggio

import com.oramind.loggio.model.DuplicateNamedRegexGroupsException
import com.oramind.loggio.util.RegexToken
import com.oramind.loggio.util.RegexUtil
import javafx.beans.property.SimpleSetProperty
import javafx.beans.property.SimpleStringProperty
import javafx.collections.FXCollections
import tornadofx.*
import java.lang.Exception

data class NamedGroupRange(val range : IntRange, val name : String, val color : String) {}
val COLORS = listOf("gray", "yellow", "orange", "cyan")

class RegexConstructorObj() {
    val regexString = SimpleStringProperty(this, "regexString")
    val textForTestingProperty = SimpleStringProperty(this, "textForTestingProperty")
    val validationErrorProperty = SimpleStringProperty(this, "validationError")
    val namedGroupsNames = SimpleSetProperty(this, "namedGroups", setOf<String>().toObservable())
//    val foundGroupRanges = simpleproperty(this, "foundGroupRanges", setOf<NamedGroupRange>().toObservable())
        val foundGroupRanges = FXCollections.observableArrayList<NamedGroupRange>()
}

class RegexConstructorViewModel : ItemViewModel<RegexConstructorObj>() {
    val regexString = bind(RegexConstructorObj::regexString)
    val textForTesting = bind(RegexConstructorObj::textForTestingProperty)
    val validationError = bind(RegexConstructorObj::validationErrorProperty)
    val foundGroupRanges = bind(RegexConstructorObj::foundGroupRanges)
}

class RegexConstructorController : Controller() {
    val textTokens = FXCollections.observableArrayList<RegexToken>()
    val viewModel : RegexConstructorViewModel by inject()

    init {
        viewModel.regexString.onChange {
            tryOrSetValidationError { reloadTokens(it.orEmpty()) }
            tryOrSetValidationError { reloadFoundRanges() }
        }
        viewModel.textForTesting.onChange {
            tryOrSetValidationError { reloadFoundRanges() }
        }
    }

    private fun tryOrSetValidationError(f : () -> Unit ) {
        try {
            f()
        } catch (exception : Exception) {
            viewModel.validationError.value = exception.message
        }
    }

    private fun reloadFoundRanges() {
        if (viewModel.textForTesting.value.isEmpty()) {
            viewModel.foundGroupRanges.value = observableListOf()
            return
        }
        val newGroups = viewModel.regexString.value
            .toRegex()
            .findAll(viewModel.textForTesting.value.orEmpty())
            .filter {
                it.value.isNotEmpty()
            }
            .mapIndexed { index, matchResult ->
                val tokensFound = textTokens.mapNotNull { token ->
                    matchResult.groups[token.tokenName]?.let {
                        Pair(token, it)
                    }
                }
                log.info("" + tokensFound.size)
                tokensFound
            }.flatten()
            .mapIndexed { index, namedMatchGroupPair ->
                val remainder = index % COLORS.size
                val range = namedMatchGroupPair.second.range
                val name = namedMatchGroupPair.first.tokenName
                val color = COLORS[remainder]
                NamedGroupRange(range, name, color)
            }
            .toList()

        viewModel.foundGroupRanges.value = newGroups.toObservable()
    }


    fun reloadTokens(string : String) {
        val newTokens = RegexUtil.groupNamesFromString(string)
        val distinctTokens = newTokens.distinct()
        if (distinctTokens.count() != newTokens.count()) {
            throw DuplicateNamedRegexGroupsException()
        }
        this.textTokens.setAll(newTokens.toList())
    }
}
