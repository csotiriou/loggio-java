package com.oramind.loggio.model.prefs

class PrefClasses {
}

data class SavedPreferences(
    val prefId: String,
    val regex : String
) {}
