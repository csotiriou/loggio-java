package com.oramind.loggio

import com.beust.klaxon.Klaxon
import com.oramind.loggio.model.AnalyzedLine
import com.oramind.loggio.model.ScalarValue
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import javafx.scene.control.TableView
import javafx.scene.layout.Priority
import org.apache.logging.log4j.LogManager
import tornadofx.*

typealias GenericJson = Map<String, Any?>
typealias JsonPair = Pair<String, Any?>
typealias ScalarValuePair = Pair<String, ScalarValue>

class MainPanelView : View("Main Panel") {
    private val mainPanelController : MainPanelController by inject()

    private val viewModel = object : ViewModel() {
        val filePath = bind { SimpleStringProperty() }
        val content = bind { SimpleStringProperty() }
        var selectedline = bind { SimpleObjectProperty<AnalyzedLine>() }
    }
    var tableView : TableView<AnalyzedLine> by singleAssign()

    override val root = vbox {

        hbox {
            button {
                text = "Parse File"
                action { parseCurrentFile() }
                hboxConstraints {
                    marginLeft = 20.0
                    marginTop = 20.0
                }
            }


            textfield(viewModel.filePath) {
                hboxConstraints {
                    hgrow = Priority.ALWAYS
                    marginLeft = 10.0
                    marginTop = 20.0
                }
            }
        }

        hbox {
            tableView = tableview(mainPanelController.fileLines) {
                log.info("constructing new line")

                hboxConstraints {
                    margin = insets(20)
                    hgrow = Priority.ALWAYS
                }

                vboxConstraints {
                    margin = insets(20)
                    vgrow = Priority.ALWAYS
                }

                onSelectionChange {
                    log.info("selected new line")
                    viewModel.selectedline.setValue(it)
                    viewModel.content.setValue(it?.content)
                }
            }


//            val parsedJson = Klaxon().parse<Map<String, Any?>>(.trimIndent())

            val strToParse = """
                {
                    "adhocDeactivation": false,
                    "bundleId": "BDLComm",
                    "bundleName": "Chat Pass",
                    "otherObject": {"one" : "two"},
                    "bundlecategories": [
                      "data"
                    ],
                    "categoryText": "Internet",
                    "channel": "VFAPP",
                    "comboValue": "\"NoComboBundle\"",
                    "commerciallyAvailable": true,
                    "confMsg": "You have chosen to purchase the Chat Pass! Would you like to continue?",
                    "durationText": "for 30 days",
                    "id": "5d8344742629e20015f56e51",
                    "initialPrice": "4.5",
                    "isFlex": false,
                    "longDescription": "For Chatting use!\r\nOffer €4,5 instead of €9!\r\nWith this bundle, you have 2GB to the following messaging services: Facebook Messenger, What’s App, Viber and Vodafone Message+ & Call+.\r\nYou must have available MB for mobile Internet to use the bundle. \r\nAccess is granted either via dedicated apps or mobile browser! \r\nCurrent bundle has priority in consumption over the above messaging services when other data bundles are also purchased. \r\nThe bundle is valid for use within the E.U. The bundle is not valid for use outside the E.U.\r\nMore info about usage policy you may find on www.vodafone.gr/vodafone-ellados/arthra/vodafone-chat-pass/.",
                    "nextBill": false,
                    "price": "with 4,5€",
                    "priority": 2,
                    "shortBundleName": "Chat Pass",
                    "shortDescription": "Για χρήση στα αγαπημένα σου Chatting apps!",
                    "teaser": "Offer with €4,5 instead of €9!"
                  }
            """
            var parsedJson = Klaxon().parse<Map<String, Any?>>(strToParse)


        }
    }

    fun refreshTable() {
        mainPanelController.columnNames.forEach { columnName ->
            tableView.column(columnName, String::class) {
                value { row -> SimpleStringProperty(row.value.properties[columnName]) }
            }
        }
    }

    fun parseCurrentFile() {
        tableView.items.clear()
        tableView.columns.clear()
        this.mainPanelController.reset()

        if (mainPanelController.columnNames.size == 0) {
            val rgex = "(?<servername>.+?) (?<servicename>.+?) (?<rest>.+)".toRegex()
            mainPanelController.parse(viewModel.filePath.value, rgex, "rest")
        }
    }

    override fun onDock() {
        super.onDock()
        viewModel.commit {
            viewModel.filePath.setValue("/Users/soulstorm/Desktop/out_subscriptions.log")
        }
    }
    companion object {
        private val log = LogManager.getLogger()
    }
}
