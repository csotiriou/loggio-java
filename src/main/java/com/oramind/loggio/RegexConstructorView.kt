package com.oramind.loggio

import com.oramind.loggio.util.RegexToken
import javafx.geometry.Insets
import javafx.scene.control.ListCell
import javafx.scene.layout.Priority
import org.apache.logging.log4j.LogManager
import org.fxmisc.richtext.InlineCssTextArea
import tornadofx.*

class RegexConstructorView : View("My View") {
    val regexConstructorController : RegexConstructorController by inject()
    val viewModel: RegexConstructorViewModel by inject()
    var codeArea : InlineCssTextArea by singleAssign()

    init {
        viewModel.foundGroupRanges.onChange {
            codeAreaRefresh()
        }
    }



    override val root = vbox {
        this.padding = Insets(10.0, 10.0, 10.0, 10.0)

        vbox {

            text("Regular Expression for each line")
            hbox {
                textfield(viewModel.regexString) {
                    hgrow = Priority.SOMETIMES
                    spacing = 10.0
                }
                button("ok") {
                    hgrow = Priority.NEVER
                }
            }
        }

        vbox {
            vboxConstraints {
                marginTop = 10.0
                marginBottom = 10.0
            }
            separator {}
        }

        vbox {
            text("Group Names Found")
            hbox {
                listview(regexConstructorController.textTokens) {
                    this.setCellFactory { object : ListCell<RegexToken>() {
                        override fun updateItem(item: RegexToken?, empty: Boolean) {
                            super.updateItem(item, empty)
                            text = if (empty || item == null) null
                            else item.tokenName
                        }
                    } }
                    prefWidth = 100.0
                    vgrow = Priority.ALWAYS
                }

                hbox {

                    hgrow = Priority.ALWAYS
                    vgrow = Priority.ALWAYS
                    padding = Insets(0.0, 10.0, 0.0, 10.0)
                    /*
                    Code Area
                     */
                    codeArea = InlineCssTextArea()
                    codeArea.hgrow = Priority.ALWAYS
                    codeArea.textProperty().onChange {
                        viewModel.textForTesting.value = it
                    }
                    this.add(codeArea)
                }
            }
        }
    }

    private fun codeAreaRefresh() {
        codeArea.clearStyle(0, viewModel.textForTesting.value.length)
        log.info("refreshing code area...")

        viewModel.foundGroupRanges.value
            .forEach {
                val range = it.range
                val color = it.color
                codeArea.setStyle(range.first, range.last + 1, "-rtfx-background-color: ${color};")
            }
    }



    companion object {
        private val logger = LogManager.getLogger()
    }
}
