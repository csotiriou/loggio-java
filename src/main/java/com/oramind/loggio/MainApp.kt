package com.oramind.loggio

import javafx.stage.Stage
import tornadofx.App

class MainApp: App(MainPanelView::class) {
    private val mainPanelController : MainPanelController by inject()

    override fun start(stage: Stage) {
        super.start(stage)
        mainPanelController.init()
        stage.width = 800.0
    }
}
