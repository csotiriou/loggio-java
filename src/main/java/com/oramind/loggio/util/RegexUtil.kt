package com.oramind.loggio.util

data class RegexToken(val tokenName : String) {
}

class RegexUtil {
    companion object {
        val GROUP_NAME_FINDER_REGEX = """\?\<(.+?)\>"""

        fun groupNamesFromString(tokenString : String): Sequence<RegexToken> {
            val regex = GROUP_NAME_FINDER_REGEX.toRegex()
            return regex
                .findAll(tokenString)
                .map { matchResult -> matchResult.groupValues.last() }
                .map { RegexToken(it) }
        }
    }
}
