package com.oramind.loggio.model

data class ParsingProperties(val regex: Regex, val filePath : String?) {}

class ParsingResults(
    lines: List<AnalyzedLine>,
    parsingProperties: ParsingProperties,
    regex: Regex
) {
    var analyzedLines = lines
    var parsingProperties = parsingProperties
}
