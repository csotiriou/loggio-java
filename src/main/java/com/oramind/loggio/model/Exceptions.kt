package com.oramind.loggio.model

import java.lang.Exception
import java.lang.RuntimeException

class DuplicateNamedRegexGroupsException() : RuntimeException("Duplicate groups found") {

}
